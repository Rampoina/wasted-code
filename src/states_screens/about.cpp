//  SuperTuxKart - a fun racing game with go-kart
//  Copyright (C) 2009-2015 Marianne Gagnon
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include "utils/string_utils.hpp"
#include "utils/constants.hpp"
#include "states_screens/about.hpp"
#include "guiengine/screen.hpp"
#include "states_screens/state_manager.hpp"
#include "states_screens/credits.hpp"

#include <fstream>
#include <algorithm>

#include "irrString.h"
using irr::core::stringw;
using irr::core::stringc;

using namespace GUIEngine;

AboutScreen::AboutScreen() : Screen("about.stkgui")
{
}   // CreditsScreen

void AboutScreen::loadedFromFile()
{
}   // loadedFromFile

void AboutScreen::eventCallback(Widget* widget, const std::string& name, const int playerID)
{
//    // Use the last used device
//    InputDevice* device = input_manager->getDeviceManager()->getLatestUsedDevice();

//    // Create player and associate player with keyboard
//    StateManager::get()->createActivePlayer(PlayerManager::getCurrentPlayer(),
//                                            device);
//    if (name == "back")
//    {
//            StateManager::get()->escapePressed();
//    }

    if (name == "back")
    {
        StateManager::get()->escapePressed();
    }
    else if (name == "credits")
    {
        // Open donation page
        CreditsScreen::getInstance()->push();
    }
    else if (name == "projecthome")
    {
      //  Online::LinkHelper::openURL(stk_config->m_donate_url);
    }
}

void AboutScreen::init()
{
    Screen::init();

    LabelWidget* game_release = getWidget<LabelWidget>("release-no");
    const irr::core::stringw &myStdString = StringUtils::utf8ToWide(STK_VERSION);
    game_release->setText(myStdString, false);
}   // init
