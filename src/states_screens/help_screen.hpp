//  SuperTuxKart - a fun racing game with go-kart
//  Copyright (C) 2009-2015 Marianne Gagnon
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef HEADER_HELP_SCREEN_HPP
#define HEADER_HELP_SCREEN_HPP

#include "guiengine/screen.hpp"
#include "unordered_map"

namespace GUIEngine { class Widget; class RibbonWidget; }

using namespace GUIEngine;

/**
  * \brief Help screen
  * \ingroup states_screens
  */
class HelpScreen : public Screen, public ScreenSingleton<HelpScreen>
{
    friend class ScreenSingleton<HelpScreen>;
    HelpScreen();
private:
    std::unordered_map<std::string, const char *> m_pages;

    RibbonWidget* m_menu_block;

    Widget* m_main_block;


    const char* const XML_NODE_MENU_ID = "category";
    const char* const XML_NODE_CONTENT_ID = "content";

    const char* m_current_page;

public:

    /** \brief implement callback from parent class GUIEngine::Screen */
    virtual void loadedFromFile() OVERRIDE;

    /** \brief implement callback from parent class GUIEngine::Screen */
    virtual void eventCallback(GUIEngine::Widget* widget, const std::string& name,
                               const int playerID) OVERRIDE;

    /** \brief implement callback from parent class GUIEngine::Screen */
    virtual void init() OVERRIDE;

    virtual const char* getPage(const char *);

    virtual void setPages(std::unordered_map<std::string, const char*>*);

    virtual void renderPage();

};

#endif
