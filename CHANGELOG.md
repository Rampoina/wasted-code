# Changelog
This file documents notable changes to Wasted across versions since its inception.

It should be kept in mind that some versions have a less complete changelog than others, and that this changelog do not list the details of the many small bugfixes and improvements which together make a significant part of the progress between releases.

For similar reasons, and because some features are vastly more complex than others, attributions of main changes should not be taken as a shortcut for overall contribution.

## Wasted 0.1.0-alpha (18. May 2023)

We are Happy to announce our first alpha release!
With this release the game finally reached a playing state.

* added 3 official cars(173-rx-mk2, 222RS, blade-xtr) and 1 official map (sky grid)
* add derby game mode
* add damage system
* changed music and added music playlists to game maps
* add server filter to show only compatible servers
* changed menu UI to access to online matches easily
* 6 new powerups
* added new help manual

### Non-game related:

* new official master server up to easily host matches at any time
* added appimage script to automatically create appimages and improved linux builder script
* server protocol version set to 0
