# WASTED!

<div align="center">

![GIF](data/gui/icons/logo_small.png)
</div>
<div align="center">

![status](https://img.shields.io/badge/status-alpha-red.svg)[![license](https://img.shields.io/badge/license-GPL--3.0-brightgreen.svg)](https://codeberg.org/franzbonanza/stk-derbymod/src/branch/main/COPYING)

[![matrix room](https://img.shields.io/badge/Chat%20on%20matrix%20-%23000.svg?&style=for-the-badge&logo=matrix&logoColor=white.svg)](https://matrix.to/%23/%23wasted:one.ems.host)

</div>

Hi! This is a mod of SuperTuxKart aiming to reproduce GTA SAMP derby destruction minigame. The game is not playable yet but I am trying to release something playable someday. I am working on the game helped by some friends and people over the internet. Spongy is helping me writing the code and Crystal is giving me his awesome car mods he is making.

![GIF](https://codeberg.org/franzbonanza/stk-derbymod/raw/commit/10e94ad63b30132e1fa56a4f52281d9557bcc472/data/preview.gif)

## Hardware Requirements
To run SuperTuxKart, make sure that your computer's specifications are equal or higher than the following specifications:

* A graphics card capable of 3D rendering - NVIDIA GeForce 470 GTX, AMD Radeon 6870 HD series card or Intel HD Graphics 4000 and newer. OpenGL >= 3.3
* You should have a dual-core CPU that's running at 1 GHz or faster.
* You'll need at least 512 MB of free VRAM (video memory).
* System memory: 1 GB
* Minimum disk space: 700 MB
* Ideally, you'll want a joystick with at least 6 buttons.

## Motivation 

Wasted was started by Franzbonanza during 2021 to fill the space left by GTA Derby Destruction, a vehicle-combat mod, which sadly became forgotten and decayed over time. 

Eventually Franz shared his ideas around which were bought with such enthusiasm that quickly people were volunteering. This would see a semi-formed team emerging. 

Namely, CrystalTheEevee donated car designs, and Spongycake submitted various code changes, amongst friends helping with testing. 

We never set out to create an entire game from scratch, but instead adhere to a community project named SuperTuxKart. SuperTuxKart's mature codebase netted us the necessary components and paved a way to focusing more time on creating fun minigames.

Lastly Wasted continues to stay loyal to open source ideals. We're putting out incremental fixes every day whilst publishing to everyone online. Additionally we invite others to help add things to the game. So check our developer channels for details. 

We can't wait to show the world a finished product! So get ready to cause destruction with just one click, no mods required!

## License
The software is released under the GNU General Public License (GPL) which can be found in the file [`COPYING`](COPYING) in the same directory as this file. Information about the licenses for the artwork is contained in `data/licenses`.

---

## 3D coordinates
A reminder for those who are looking at the code and 3D models:

SuperTuxKart: X right, Y up, Z forwards

Blender: X right, Y forwards, Z up

The export utilities  perform the needed transformation, so in Blender you just work with the XY plane as ground, and things will appear fine in STK (using XZ as ground in the code, obviously).
## Building from source

You can try our new helper script in the folder of the game (stkHelper.sh).
That would be the easiest way to compile the game and start it. Keep in mind that it's an experimental "launcher" and works only for Debian based distros currently!
If you wish to do that manually you can follow the Building instructions in [`INSTALL.md`](INSTALL.md)

## Reach us!
Connect with us on matrix: https://matrix.to/#/#wasted:one.ems.host

## Contributors

These are the important people whom aided in bringing the game to life.

### Programmers
Lead Developer - Franzbonanza

Developer -- Spongycake

### Visuals 
Race cars -- CrystalTheEevee

----

Special thanks to everyone working on SuperTuxKart. Their work has made our game possible. The story goes that Wasted used SuperTuxKart as a launchpad. For more details, visit [supertuxkart.net](https://supertuxkart.net/Team).
