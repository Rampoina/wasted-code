set(FILE_HEADER "This file is only here to help STK finding the right directory,\nand to avoid problems with other directories called 'data'.\n")
set(VERSION_PART "MINOR")

message(STATUS "PROJECT_VERSION: ${NEW_VERSION}")
if(NOT DEFINED NEW_VERSION)
    message(STATUS "Relying on git to retrieve the lastest release tag.")
    execute_process(
        COMMAND git describe --tags --abbrev=0 
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
	OUTPUT_VARIABLE RECENT_GIT_TAG
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif()

if(DEFINED RECENT_GIT_TAG)
  message("Lastest git version: ${RECENT_GIT_TAG}")
  if (VERSION_PART STREQUAL "MAJOR")
    set(regex_pattern "^[0-9]+")
  elseif (VERSION_PART STREQUAL "MINOR")
     set(regex_pattern "\\.([0-9]+)")
  elseif (VERSION_PART STREQUAL "PATCH")
    set(regex_pattern "^[0-9]+\\.[0-9]+\\.([0-9]+)")
  endif()

  string(REGEX MATCH "${regex_pattern}" version_match ${RECENT_GIT_TAG})

  if (NOT version_match STREQUAL "")

    message("New version: ${version_match}")
    string(REPLACE "." "" version_match ${version_match})

    message("after replace version: ${version_match}")
    string(SUBSTRING ${version_match} 0 -1 version_number)

    message("New version: ${version_number}")
    set(bump_version_number ${version_number})
    math(EXPR bump_version_number "${bump_version_number}+1")

    message("${VERSION_PART} version: ${version_number} bumped to ${bump_version_number}")
    string(REPLACE "${version_number}" "${bump_version_number}" NEW_VERSION "${RECENT_GIT_TAG}")
    message("New version: ${NEW_VERSION}")
  else()
    message("No match found.")
  endif()
endif()

string(REGEX MATCH "^[0-9]+\\." VERSION_PREFIX ${NEW_VERSION})

# Shave anything off after 5th character
string(SUBSTRING "${NEW_VERSION}" 0 5 NEW_VERSION)

message(STATUS "PROJECT_VERSION: ${NEW_VERSION}")

if(NOT VERSION_PREFIX)
     message(STATUS "Invalid version number format. Must be in X.Y.Z format. Defaulting version to git instead.")
     set(NEW_VERSION "git")
endif()

message(STATUS ${PROJECT_SOURCE_DIR})

if(NOT DEFINED PROJECT_SOURCE_DIR)
	set(PROJECT_SOURCE_DIR "..")
endif()

set(VERSION_FILE ${PROJECT_SOURCE_DIR}/data/supertuxkart.${NEW_VERSION})
message(${VERSION_FILE})
if(EXISTS ${VERSION_FILE}) 
	message("Version file was previously created! Skipping version file creation step...")
else() 
	message("File \"supertuxkart.${NEW_VERSION}\" does not exist. Attemping to create..") 
	file(WRITE ${PROJECT_SOURCE_DIR}/data/supertuxkart.${NEW_VERSION} "${FILE_HEADER}")
	if(EXISTS ${VERSION_FILE})
		message("Successfully created file.")
	else()
		message("Writing file error.") 
	endif()
endif()
message("Your version is set at ${VERSION_FILE}") 
