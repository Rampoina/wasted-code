#!/bin/sh

APP=wasted

mkdir tmp;
cd ./tmp;
wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
mv ./appimagetool-x86_64.AppImage ./appimagetool
chmod a+x ./appimagetool
mkdir $APP.AppDir
cp ../../build-linux-install/Wasted-*-linux-64bit.tar.xz ./
tar xf ./*.tar.xz;
mv ./SuperTuxKart-*-linux-*/* ./$APP.AppDir

echo "[Desktop Entry]
Name=Wasted
Icon=wasted
GenericName=A Derby destruction online game
GenericName[da]=Et kart racerspil
GenericName[de]=Ein Kart-Rennspiel
GenericName[fr]=Un jeu de karting
GenericName[gl]=Xogo de carreiras con karts
GenericName[pl]=Wyścigi gokartów
GenericName[ro]=Un joc de curse cu carturi
Exec=supertuxkart
Terminal=false
StartupNotify=false
Type=Application
Categories=Game;ArcadeGame;
Keywords=cars;game;derby;" >> ./$APP.AppDir/$APP.desktop;
cp ./$APP.AppDir/data/supertuxkart_512.png ./$APP.AppDir;
mv ./$APP.AppDir/supertuxkart_512.png ./$APP.AppDir/$APP.png
mv ./$APP.AppDir/run_game.sh ./$APP.AppDir/AppRun;

ARCH=x86_64 ./appimagetool -n ./$APP.AppDir;

cd ..;
mv ./tmp/*mage ../build-linux-install/$APP-64bit.AppImage;
rm -r ./tmp

echo "";
echo "   Wasted is provided by https://wastedgames.codeberg.page/";
echo "";
echo " This AppImage is created using the official build for Linux";
echo "";
